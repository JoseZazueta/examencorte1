/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.examenprimercorte;

/**
 *
 * @author zazue
 */
public abstract class Empleado {
    protected  int NumEmpleado;
    protected String Nombre;
    protected String Domicilio;
    protected int PagoporDia;
    protected int DiasTrabajados;
    //Constructores
    public Empleado(int NumEmpleado,String Nombre,String Domicilio,int PagoporDia,int DiasTrabajados){
        this.NumEmpleado = NumEmpleado;
        this.Nombre = Nombre;
        this.Domicilio = Domicilio;
        this.PagoporDia = PagoporDia;
        this.DiasTrabajados = DiasTrabajados;
    }
    public Empleado(){
        this.NumEmpleado = 0;
        this.Nombre = "";
        this.Domicilio = "";
        this.PagoporDia=0;
        this.DiasTrabajados=0;
    }
    //set & get

    public int getNumEmpleado() {
        return NumEmpleado;
    }

    public void setNumEmpleado(int NumEmpleado) {
        this.NumEmpleado = NumEmpleado;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDomicilio() {
        return Domicilio;
    }

    public void setDomicilio(String Domicilio) {
        this.Domicilio = Domicilio;
    }

    public int getPagoporDia() {
        return PagoporDia;
    }

    public void setPagoporDia(int PagoporDia) {
        this.PagoporDia = PagoporDia;
    }

    public int getDiasTrabajados() {
        return DiasTrabajados;
    }

    public void setDiasTrabajados(int DiasTrabajados) {
        this.DiasTrabajados = DiasTrabajados;
    }
    public abstract float CalcularTotal();
    public abstract float CalcularImpuestos();
}
