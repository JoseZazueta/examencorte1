/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.examenprimercorte;

/**
 *
 * @author zazue
 */
public class EmpleadoAdministrativo extends Empleado implements Impuestos {
    private String TipoContrato;
    //Constructor
    public EmpleadoAdministrativo(String Nombre,String Domicilio,int PagoporDia,int DiasTrabajados,String TipoContrato){
        this.TipoContrato=TipoContrato;
    }
    public EmpleadoAdministrativo(){
        this.TipoContrato="";
    }
    //set&get

    public String getTipoContrato() {
        return TipoContrato;
    }

    public void setTipoContrato(String TipoContrato) {
        this.TipoContrato = TipoContrato;
    }

    @Override
    public float CalcularTotal() {
       float pago = 0;
       return this.PagoporDia*this.DiasTrabajados;
    }

    @Override
    public float CalcularImpuestos() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
