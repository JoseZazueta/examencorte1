/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.examenprimercorte;

/**
 *
 * @author zazue
 */
public interface Impuestos {
    public abstract float CalcularTotal();
}
